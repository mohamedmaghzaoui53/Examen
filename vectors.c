#include<stdio.h>
#include<stdlib.h>
#include<math.h>
void add_vectors(int n, double *t1, double *t2, long double *t3){
    for (int i = 0; i < n; i++)
    {
        t3[i]=((t1[i]-t2[i])*(t1[i]-t2[i]));
    }
    
}
double norm_vector(int n, double *t){
    double norme=0;
    for (int i = 0; i < n; i++)
    {
        norme+=t[i]*t[i];
    }
    norme=sqrt(norme);
    return norme;
}

int main() {
	double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
	double T2[] = { 2.71,  2.5,  -1, 3, -7  };
	long double T3[5];
	int n = 5;
    add_vectors(n,T1,T2,T3);
	printf("T1 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T1[i]);
	}
	printf("\nT2 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T2[i]);
	}
	printf("\n");
    printf("\nT3 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2Lf\n ", T3[i]);
	}
	
	exit(EXIT_SUCCESS);
}
