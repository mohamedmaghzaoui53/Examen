1:Les étapes nécessaires pour supprimer un élément d'une liste chaînée en connaissant son adresse sont les suivantes est de Parcourir la liste jusqu'à atteindre l'élément à supprimer apres de Modifier les pointeurs pour supprimer l'élément de la liste et finalement de Libérer la mémoire allouée à l'élément supprimé.


2:Le difference notable entre les  deux fonctions est que  La première fonction *remove_list_entry_bad_taste parcourt la liste pour trouver l'élément à supprimer en utilisant une boucle while. La seconde fonction remove_list_entry_good_taste  utilise un pointeur vers un autre pointeur  qui pointe vers l'élément à supprimer.



3:La seconde fonction doit renvoyer une valeur pour mettre à jour la tête de la liste. En effet, la fonction peut supprimer la tête de la liste, ce qui nécessite de renvoyer la nouvelle tête de la liste.CE pointeur vers un pointeur est utilisé pour pouvoir modifier le pointeur lui-même et pas seulement la valeur pointée Dans le cas d'une liste chaînée, cela permet de modifier le pointeur qui pointe vers l'élément à supprimer, plutôt que de modifier l'élément lui-même.



4:La seconde fonction reçoit un pointeur vers un pointeur comme argument car elle modifie l'adresse du pointeur qui pointe vers l'élément à supprimer. 



5:La seconde fonction remove_list_entry_good_taste est plus élégante car elle utilise un pointeur vers un pointeur et ca permet d'optimiser le performance




