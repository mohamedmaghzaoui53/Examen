#include <stdio.h>
int count_char(char *string, char c){
    int i=0;
    int countsC=0;
    while (string[i]!='\0')
    {
        if (string[i]==c){
            countsC++;
        }
        i++;
    }
    return countsC;
    
}
int count_words(char *string){
    int i=0;
    int countW=1;
    while (string[i]!='\0')
    {
        if (string[i]==' ')
        {
            countW++;
        }
        i++;
        if (string[i]==' '&&string[i+1]==' ')
        {
            return 0;
            break;
        }
        
        
    }
    return countW;
    
}
int count_words_better(char *string){
    int i=0;
    int countW=1;
    while (string[i]!='\0')
    {
        if (string[i]==' ' && string[i+1]!=' ')
        {
            countW++;
        }
        i++;

        
    }
    return countW;
}

int main(){
    char string[50];
    printf("string: ");
    scanf("%[^\n]",string);

    printf(" caracter count= %d\n",count_char(string,'o'));
    if (count_words(string)==0)
    {
        printf("normal world count= error\n");
    }else{
        printf(" normal word count= %d\n",count_words(string));
    }
    
    printf(" good word count= %d\n",count_words_better(string));

    return 0;
}