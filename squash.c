#include <stdio.h>
void small_to_zero(int *t, int n, int val){
    for (int i = 0; i < n; i++)
    {
        if (t[i]<=val){
            t[i]=0;
        }
    }
}
int main(){
    int tab[] = { 1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0 };
    small_to_zero(tab,11,4);
    for (int i = 0; i < 11; i++)
    {
        printf("%d \n",tab[i]);
    }   
}