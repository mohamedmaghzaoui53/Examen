.PHONY: all clean

PROGS=vectors intlist linus prices intlist counts squash
CC=gcc
CFLAGS=
LDFLAGS=-lm

all: $(PROGS)

%: %.c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

%.s: %.c
	$(CC) -S $<

clean:
	rm -f $(PROGS) *.s

	
